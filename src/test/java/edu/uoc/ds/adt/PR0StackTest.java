package edu.uoc.ds.adt;

import org.junit.After;
import org.junit.Before;

import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class PR0StackTest {

    PR0Stack pr0q;

    @Before
    public void setUp() {
        this.pr0q = new PR0Stack();

        assertNotNull(this.pr0q.getStack());
    }

    @After
    public void release() {
        this.pr0q = null;
    }


    @org.junit.Test
    public void stackTest() {
        this.pr0q.fillStack();
        assertEquals(Integer.valueOf(this.pr0q.getStack().size()), Integer.valueOf(this.pr0q.CAPACITY));
        // original clearFullQueue method
        assertEquals(this.pr0q.clearAllStack(), new String("8 7 6 5 4 3 2 1 0 "));
        // we need to fill the que in order to test the new method
        this.pr0q.fillStack();
        // clearFullQueue method based on Integer
        assertEquals(this.pr0q.clearAllStackWithInteger(), Arrays.asList(8,7,6,5,4,3,2,1,0));
        assertEquals(Integer.valueOf(this.pr0q.getStack().size()), Integer.valueOf(0));
    }
}
