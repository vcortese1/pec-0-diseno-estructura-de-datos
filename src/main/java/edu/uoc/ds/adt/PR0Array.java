package edu.uoc.ds.adt;

public class PR0Array {

    public final int CAPACITY = 50;

    private int[] array;

    public PR0Array(){
        array = new int[CAPACITY];
        for(int i = 0; i<CAPACITY; i ++){
            this.array[i] = i*2;
        }
    }

    public int[] getArray(){
        return array;
    }

    public int getIndexOf(int numberToFind){
        int result = -1;
        int iteractions = 0;
        for(int i = 0; i<CAPACITY && result == -1; i ++){
            if(array[i] == numberToFind) result = i;
            iteractions++;
        }
        System.out.println("[Búsqueda secuencial]["+ numberToFind +"]"+" [" + iteractions +" iteraciones]");
        return  result;
    }

    public int binarySearch(int numberToFind){
        int index = (CAPACITY/2) -1;
        int midValue;
        int result = -1;
        boolean condition = (result == -1);
        boolean isGoingUp = false;
        int iteractions = 0;

        for(int i = index; condition; i = (isGoingUp) ? i+1 : i-1)
        {
            midValue = array[i];
            if(midValue == numberToFind) result = i;
            if(i ==(CAPACITY/2) -1 ){
                if(numberToFind > midValue){
                    isGoingUp = true;
                }
            }
            if(isGoingUp) condition = (result == -1 && index < CAPACITY && midValue < numberToFind);
            else condition = (result == -1 && index >= 0 && midValue > numberToFind);
            iteractions++;
        }
        System.out.println("[Búsqueda binaria]["+ numberToFind +"]"+" [" + iteractions +" iteraciones]");
        return result;
    }
}
