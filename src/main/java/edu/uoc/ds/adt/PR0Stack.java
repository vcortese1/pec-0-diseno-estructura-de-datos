package edu.uoc.ds.adt;


import edu.uoc.ds.adt.sequential.Stack;
import edu.uoc.ds.adt.sequential.StackArrayImpl;

import java.util.ArrayList;
import java.util.List;

public class PR0Stack {

    // Capacitat màxima de la seqüència.
    public final int CAPACITY = 9;

    private Stack<Integer> stack;

    public PR0Stack() {
        newStack();
    }

    public void newStack() {
        stack = new StackArrayImpl<Integer>(CAPACITY);
    }


    public void fillStack() {
        for (Integer n = 0; n < 9; n++) {
            stack.push(n);
        }
    }

    public String clearAllStack() {
        StringBuilder sb = new StringBuilder();
        while (!stack.isEmpty())
            sb.append(stack.pop()).append(" ");
        return sb.toString();
    }

    // A new method has been added in order to test de que values without using String or characters.
    // For this purpose the java.List library will do the job
    // This method has been added to the test file
    public List<Integer> clearAllStackWithInteger() {
        List sb = new ArrayList<Integer>();
        while (!stack.isEmpty())
            sb.add(stack.pop());
        return sb;
    }

    public Stack<Integer> getStack() {
        return this.stack;
    }
}
