package edu.uoc.ds.adt;

import edu.uoc.ds.adt.sequential.Queue;
import edu.uoc.ds.adt.sequential.QueueArrayImpl;

import java.util.ArrayList;
import java.util.List;

public class PR0Queue {

    public final int CAPACITY = 9;

    private Queue<Integer> queue;

    public PR0Queue() {
        newQueue();
    }
    public void newQueue() {
        queue = new QueueArrayImpl<>(CAPACITY);
    }

    public void fillQueue() {
        for (Integer n = 0; n < 9; n++) {
            queue.add(n);
        }
    }

    public String clearFullQueue() {
        StringBuilder sb = new StringBuilder();
        while (!queue.isEmpty()) {
            sb.append(queue.poll()).append(" ");
        }
        return sb.toString();
    }

    // A new method has been added in order to test de que values without using String or characters.
    // For this purpose the java.List library will do the job
    // This method has been added to the test file
    public List<Integer> clearFullQueueWithInteger() {
        List sb = new ArrayList<Integer>();
        while (!queue.isEmpty()) {
            sb.add(queue.poll());
        }
        return sb;
    }

    public Queue<Integer> getQueue() {
        return this.queue;
    }

}
